package events.vandal.eegg.utils

import org.bukkit.Location
import org.bukkit.World
import java.util.*

data class Egg(
    val id: String,
    val name: String,
    var type: EggTypes,
    val position: Location,
    val world: World,
    val acquired: MutableList<UUID>
)

/*
Present file structure :

presents:
    - id: 4a92b33f
      name: "Void Hollow"
      type: 0
      position:
        - 127
        - 42
        - -446
      world: "minecraft:overworld"
      acquired:
        - 031c8ca803b141a3a745fc0415a67adf # BluSpring_YT
 */