package events.vandal.eegg.utils

enum class EggTypes(val value: Int) {
    GREEN(0),
    CYAN(1),
    GOLD(2),
    GREEN2(3),
    BLUE(4),
    RED(5);

    companion object {
        fun fromInt(value: Int) = EggTypes.values().first { it.value == value }
    }
}