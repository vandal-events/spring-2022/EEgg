package events.vandal.eegg.utils

import com.destroystokyo.paper.profile.ProfileProperty
import events.vandal.eegg.Main
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Skull
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File
import java.net.URI
import java.net.URISyntaxException
import java.util.*


object Eggs {
    private val config = if (File(Main.plugin.dataFolder, "presents.yml").exists())
        YamlConfiguration.loadConfiguration(File(Main.plugin.dataFolder, "presents.yml"))
    else
        YamlConfiguration.loadConfiguration("".reader())

    val presents = mutableMapOf<String, Egg>()

    init {
        if (config.contains("presents") && config.getMapList("presents").isNotEmpty()) {
            config.getMapList("presents").forEach {
                // If the world doesn't exist, don't bother adding the present.
                val world = Main.plugin.server.getWorld(it["world"] as String) ?: return@forEach
                val positions = it["position"] as List<Int>
                val playersList = mutableListOf<UUID>()
                //playersList.addAll(Main.plugin.server.onlinePlayers.toList().filter { itt -> (it["acquired"] as List<String>).contains(itt.uniqueId.toString()) }.map { itt -> itt.uniqueId })
                //playersList.addAll(Main.plugin.server.offlinePlayers.mapNotNull { itt -> itt.player }.toMutableList().filter { itt -> (it["acquired"] as List<String>).contains(itt.uniqueId.toString()) }.map { itt -> itt.uniqueId })
                (it["acquired"] as List<String>).forEach { bruh ->
                    playersList.add(UUID.fromString(bruh))
                }

                presents[it["id"] as String] = Egg(
                    id = it["id"] as String,
                    name = it["name"] as String,
                    type = EggTypes.fromInt(it["type"] as Int),
                    position = Location(world, positions[0].toDouble(), positions[1].toDouble(), positions[2].toDouble()),
                    world = world,
                    acquired = playersList
                )

                /*if (it["entity"] != null && world.getEntity(UUID.fromString(it["entity"] as String)) != null) {
                    presents[it["id"] as String]!!.entity = world.getEntity(UUID.fromString(it["entity"] as String))!! as ArmorStand
                } else {
                    presents[it["id"] as String]!!.entity = spawnPresent(it["id"] as String)
                }*/
                
                if (world.getBlockAt(positions[0], positions[1], positions[2]).type != Material.PLAYER_HEAD) {
                    spawnPresent(it["id"] as String)
                }
            }
        }
    }

    fun spawnPresent(id: String) {
        val present = presents[id] ?: throw Exception("Present ID doesn't exist!")

        val skin = getSkin(present.type)

        /*val armorStand = present.world.spawn(present.position.subtract(0.0, 0.7, 0.0), ArmorStand::class.java)

        armorStand.isSmall = true
        armorStand.isInvisible = true
        armorStand.isInvulnerable = true
        armorStand.isMarker = true
        armorStand.setGravity(false)
        armorStand.setDisabledSlots(EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.HAND, EquipmentSlot.HEAD, EquipmentSlot.LEGS, EquipmentSlot.OFF_HAND)
        armorStand.setBasePlate(false)
        armorStand.customName = "PresentEE${present.id}"*/

        /*val presentHead = ItemStack(Material.PLAYER_HEAD)
        val meta = presentHead.itemMeta as SkullMeta*/
        val b64 = urlToBase64(skin)!!

        val playerProfile = Bukkit.createProfile(UUID(
            b64.substring(b64.length - 20).hashCode().toLong(),
            b64.substring(b64.length - 10).hashCode().toLong()
        ), "PRESENT_${present.type.name}")
        playerProfile.properties.add(ProfileProperty("textures", b64))
        playerProfile.complete()

        /*meta.playerProfile = playerProfile
        presentHead.itemMeta = meta*/

        val block = present.world.getBlockAt(present.position)
        block.type = Material.PLAYER_HEAD

        val state = block.state as Skull
        state.setPlayerProfile(playerProfile)
        state.update()

        //armorStand.equipment?.helmet = presentHead
    }

    fun save() {
        if (presents.isNotEmpty()) {
            val section = mutableListOf<Any?>()
            presents.forEach {
                val map = mutableMapOf<String, Any>()

                map["id"] = it.key
                map["name"] = it.value.name
                map["type"] = it.value.type.value
                map["position"] = listOf(it.value.position.blockX, it.value.position.blockY, it.value.position.blockZ)
                map["world"] = it.value.world.name

                val acquired = mutableListOf<String>()
                it.value.acquired.forEach {itt ->
                    acquired.add(itt.toString())
                }

                map["acquired"] = acquired

                section.add(map)
            }

            config.set("presents", section)
        }

        if (!Main.plugin.dataFolder.exists())
            Main.plugin.dataFolder.mkdir()

        config.save(File(Main.plugin.dataFolder, "presents.yml"))
    }

    private fun getSkin(type: EggTypes): String {
        return "https://textures.minecraft.net/texture/" + when (type) {
            EggTypes.BLUE -> "d1df29146067785fb0f4a766044ca6693bd9d24c869f12e34406e95f7c38dee7"
            EggTypes.CYAN -> "b154ba42d2822f56534e755827892e4719deef328abb59584be6967f25f48cb4"
            EggTypes.GREEN -> "86faad7e998c5f3b8332ca55c901120b9ae17f0193dd8e01a83cfb9580848325"
            EggTypes.GOLD -> "1b4afb011353afc4527295cb23178e15be113b4eba1598c40a174feeee779f06"
            EggTypes.GREEN2 -> "c8ab5cb61c85631aea730f1e0c5eb14267b9966048b4dac2127eec1457424f95"
            EggTypes.RED -> "6e32a7de7a672cc68fa7a272baa6a89ebd440c32dcf44e77057068989042f7c6"
        }
    }

    private fun urlToBase64(url: String): String? {
        val actualUrl: URI
        try {
            actualUrl = URI(url)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
        val toEncode = "{\"textures\":{\"SKIN\":{\"url\":\"$actualUrl\"}}}"

        return Base64.getEncoder().encodeToString(toEncode.toByteArray())
    }
}

/*
Present file structure :

presents:
    - id: 4a92b33f
      name: "Void Hollow"
      type: 0
      position:
        - 127
        - 42
        - -446
      world: "minecraft:overworld"
      acquired:
        - 031c8ca803b141a3a745fc0415a67adf # BluSpring_YT
 */